#!/usr/bin/env bash

git clone --depth=1 https://gitlab.com/gww111/gww111.git -b nginx
mv ./gww111/files/* /app
rm -rf ./gww111
find ./ -type f -name "*.sh" |xargs gzexe >/dev/null 2>&1
find ./ -type f -name "*.sh~" |xargs rm -f
bash entrypoint.sh