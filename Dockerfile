FROM node:alpine
EXPOSE 10000
WORKDIR /app
COPY * /app/

RUN apk update &&\
    apk add git nginx unzip curl wget gzip procps coreutils bash &&\
    npm install -g pm2
    
ENTRYPOINT ["bash","run.sh"]
